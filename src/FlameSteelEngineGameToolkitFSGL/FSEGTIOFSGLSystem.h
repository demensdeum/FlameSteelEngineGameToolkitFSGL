/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSEGTIOFSGLSystem.h
 * Author: demensdeum
 *
 * Created on August 5, 2017, 6:25 PM
 */

#ifndef FSEGTIOFSGLSYSTEM_H
#define FSEGTIOFSGLSYSTEM_H

#include <SDL2/SDL.h>
#include <FlameSteelCommonTraits/Screenshot.h>
#include <FlameSteelCommonTraits/IOSystemParams.h>
#include <FlameSteelEngineGameToolkitFSGL/Renderers/FSEGTIOFSGLSystemRenderer.h>
#include <FlameSteelEngineGameToolkit/IO/IOSystems/FSEGTIOSystem.h>
#include <memory>
#include <FSGL/Data/SystemParams.h>
#include <FSGL/Data/ModelsLibrary/ModelsLibrary.h>

class FSEGTIOFSGLSystem: public FSEGTIOSystem, public FSEGTIOFSGLSystemRendererDelegate {
public:
    FSEGTIOFSGLSystem();
    FSEGTIOFSGLSystem(const FSEGTIOFSGLSystem& orig);
    virtual ~FSEGTIOFSGLSystem();

    virtual void preInitialize();
    virtual void fillParams(shared_ptr<IOSystemParams> params);
    virtual void initialize(shared_ptr<IOSystemParams> params);

	void fsglSystemRendererDidTapButton(shared_ptr<FSEGTIOFSGLSystemRenderer> renderer, shared_ptr<Button> button);

    virtual void objectsContextObjectAdded(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object);
    virtual void objectsContextObjectUpdate(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object);
    virtual void objectsContextAllObjectsRemoved(shared_ptr<FSEGTObjectsContext> context);
    virtual void objectsContextObjectRemoved(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object);
    void setWindowTitle(string title);

    shared_ptr<Screenshot> takeScreenshot();

    shared_ptr<MaterialLibrary> materialLibrary = make_shared<MaterialLibrary>();
	NotNull<ModelsLibrary> modelsLibrary = make<ModelsLibrary>();

    SDL_Window *window;

private:
    shared_ptr<FSEGTIOFSGLSystemRenderer> fsglRenderer;
};

#endif /* FSEGTIOFSGLSYSTEM_H */

