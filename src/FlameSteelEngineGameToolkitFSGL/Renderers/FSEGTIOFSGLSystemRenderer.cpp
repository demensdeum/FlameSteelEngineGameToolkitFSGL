/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSEGTIOFSGLSystemRenderer.cpp
 * Author: demensdeum
 *
 * Created on August 5, 2017, 6:45 PM
 */

#include "FSEGTIOFSGLSystemRenderer.h"
#include <FlameSteelEngineGameToolkit/Const/FSEGTConst.h>

#include <iostream>
#include <FlameSteelEngineGameToolkit/Data/Components/Text/FSEGTText.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <FlameSteelCore/FSCUtils.h>
#include <FlameSteelEngineGameToolkitFSGL/FSEGTIOFSGLSystem.h>

#include <FlameSteelEngineGameToolkitFSGL/Data/FSGTIOFSGLSystemFactory.h>

using namespace FlameSteelCore::Utils;
using namespace Shortcuts;

FSEGTIOFSGLSystemRenderer::FSEGTIOFSGLSystemRenderer() {
}

void FSEGTIOFSGLSystemRenderer::preInitialize() {
    controller = make_shared<FSGLController>();
	controller->delegate = shared_from_this();
    controller->preInitialize();
}

void FSEGTIOFSGLSystemRenderer::setWindowTitle(string title) {
    SDL_SetWindowTitle(window, title.c_str());
}

void FSEGTIOFSGLSystemRenderer::fillParams(shared_ptr<IOSystemParams> params) {
    if (controller.get() == nullptr) {
        throwRuntimeException(string("Should initialize FSGL controller first - stop"));
    }

    this->params = dynamic_pointer_cast<SystemParams>(params);
    controller->fillParams(this->params.sharedPointer());
}

void FSEGTIOFSGLSystemRenderer::fsglControllerDidTapButton(shared_ptr<FSGLController> , shared_ptr<Button> button) {
	if (delegate == nullptr) {
		cout << "Can't send did tap button event, because system renderer delegate is nullptr" << endl;
		return;
	}
	delegate->fsglSystemRendererDidTapButton(shared_from_this(), button);
}

void FSEGTIOFSGLSystemRenderer::initializeWindow(shared_ptr<IOSystemParams> params) {
    if (controller.get() == nullptr) {
        throwRuntimeException(string("Should initialize FSGL controller first - stop"));
    }

    if (params.get() == nullptr) {
        throwRuntimeException(string("Can't initialize FSGLSystemRenderer - params is null"));
    }

    this->params = dynamic_pointer_cast<SystemParams>(params);
    window = controller->initializeWindow(this->params.sharedPointer());
}

void FSEGTIOFSGLSystemRenderer::render(shared_ptr<FSEGTGameData> ) {

    controller->render();

}

void FSEGTIOFSGLSystemRenderer::objectsContextObjectAdded(shared_ptr<FSEGTObjectsContext>, shared_ptr<Object> object) {

    cout << "FSEGTIOFSGLSystemRenderer: object add - " << object->getInstanceIdentifier()->c_str() << endl;

    if (object->getClassIdentifier().get() == nullptr) {

        cout << "FSEGTIOFSGLSystemRenderer: empty name for added object - exit" << endl;

        exit(1);

    }

    if (FSEGTUtils::contains3D(object)) {

        auto fsglIOSystem = dynamic_pointer_cast<FSEGTIOFSGLSystem>(ioSystem);
        auto materialLibrary = fsglIOSystem->materialLibrary;
	auto modelsLibrary = fsglIOSystem->modelsLibrary;

        if (materialLibrary.get() == nullptr) {
            throwRuntimeException(string("Material Library nullptr - stop"));
        }

        auto t1 = std::chrono::high_resolution_clock::now();

        auto graphicsObject = FSGTIOFSGLSystemFactory::graphicsObjectFrom(object, materialLibrary, modelsLibrary);
        controller->addObject(graphicsObject);

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
        std::cout << "Model loading duration: " << duration << endl;

        int const tooLong = 10000;

        if (params->raiseExecutionWhenModelLoadingTooLong && duration > tooLong) {
            if (object->containsComponentWithIdentifier(make_shared<string>(FSEGTConstComponentsModel)))
            {
                auto modelFilePath = FSEGTUtils::getModelFilePathForObject(object);
                string errorText = "Model loading time is too long! Stop: ";
                errorText += modelFilePath->c_str();
                errorText += " (";
                errorText += to_string(duration);
                errorText += ")";
                throwRuntimeException(errorText);
            }
        }


    } else if (object->getClassIdentifier()->compare("ui") == 0) {

    } else if (object->getClassIdentifier()->compare("camera") == 0) {

        auto position = FSEGTUtils::getObjectPosition(object);
        auto rotation = FSEGTUtils::getObjectRotation(object);

        controller->setCameraX(position->x);
        controller->setCameraY(position->y);
        controller->setCameraZ(position->z);

        controller->setCameraRotationX(rotation->x);
        controller->setCameraRotationY(rotation->y);
        controller->setCameraRotationZ(rotation->z);

    }
	else if (object->getClassIdentifier()->compare(FSEGTConstObjectClassIdentifierOnScreenButton) == 0) {
		auto instanceIdentifier = object->getInstanceIdentifier();
		auto title = FSEGTUtils::getText(object);
		auto id = *instanceIdentifier;
		auto button = make_shared<Button>();
		button->title = *title->text.get();
		button->id = id;
		button->uuid = object->uuid;
		controller->addButton(button);
	}
    else {
        throwRuntimeException(string("Can't add object to system renderer, not enough components to represent; If this is 3D model then probably incorrect model path."));
    }
}

void FSEGTIOFSGLSystemRenderer::objectsContextObjectUpdate(shared_ptr<FSEGTObjectsContext>, shared_ptr<Object> object) {

    if (object->containsComponentWithIdentifier(make_shared<string>("currentAnimation"))) {

        auto textSharedPointer = object->getComponent(make_shared<string>("currentAnimation"));
        auto animationName = static_pointer_cast<FSEGTText>(textSharedPointer)->text;

        auto id = object->uuid;
        auto graphicsObject = controller->getObjectWithID(id);
        graphicsObject->playAnimationWithName(animationName);
    }

    if (FSEGTUtils::contains3D(object)) {

        auto position = FSEGTUtils::getObjectPosition(object);
        auto rotation = FSEGTUtils::getObjectRotation(object);
	auto scale = FSEGTUtils::getObjectScale(object);

        auto brightness = FSEGTUtils::getObjectBrightness(object);
        auto isVisible = FSEGTUtils::getObjectIsVisible(object);

        auto id = object->uuid;
        auto graphicsObject = controller->getObjectWithID(id);

        if (graphicsObject.get() == nullptr)
        {
            throwRuntimeException(string("Can't update object - graphics object - null"));
        }

        graphicsObject->positionVector->x = position->x;
        graphicsObject->positionVector->y = position->y;
        graphicsObject->positionVector->z = position->z;

        graphicsObject->rotationVector->x = rotation->x;
        graphicsObject->rotationVector->y = rotation->y;
        graphicsObject->rotationVector->z = rotation->z;

	graphicsObject->scaleVector->x = scale->x;
	graphicsObject->scaleVector->y = scale->y;
	graphicsObject->scaleVector->z = scale->z;

        graphicsObject->brightness = brightness->floatNumber;
        graphicsObject->isVisible = isVisible;

        if (object->containsComponentWithIdentifier(make_shared<string>("Overlap Object")) == true) {
            graphicsObject->overlapObject = true;
        }
        else {
            graphicsObject->overlapObject = false;
        }

        if (object->containsComponentWithIdentifier(make_shared<string>("Should Play Animation"))) {
            auto animationComponent = object->getComponent(make_shared<string>("Should Play Animation"));
            auto animationName = animationComponent->getInstanceIdentifier();
            graphicsObject->playAnimationWithName(animationName);
            object->removeComponent(make_shared<string>("Should Play Animation"));
        }

    } else if (object->getClassIdentifier()->compare("camera") == 0) {

        auto position = FSEGTUtils::getObjectPosition(object);
        auto rotation = FSEGTUtils::getObjectRotation(object);

        controller->setCameraX(position->x);
        controller->setCameraY(position->y);
        controller->setCameraZ(position->z);

        controller->setCameraRotationX(rotation->x);
        controller->setCameraRotationY(rotation->y);
        controller->setCameraRotationZ(rotation->z);

    }

}

void FSEGTIOFSGLSystemRenderer::objectsContextAllObjectsRemoved(shared_ptr<FSEGTObjectsContext> ) {

    ////cout << "FSEGTIOFSGLSystemRenderer: all objects removed" << endl;

    controller->removeAllObjects();

}

void FSEGTIOFSGLSystemRenderer::objectsContextObjectRemoved(shared_ptr<FSEGTObjectsContext>, shared_ptr<Object> object)
{
    auto id = object->uuid;
    auto graphicsObject = controller->getObjectWithID(id);

	if (graphicsObject.get() == nullptr) {
		auto button = controller->getButtonWithUUID(id);
		if (button.get() == nullptr) {
			cout << "Can't remove graphics object that was never rendered, check your scene logic" << endl;
		}
		else {
			controller->removeButton(button);
		}
	}
	else {
    		controller->removeObject(graphicsObject);
	}
}

shared_ptr<Screenshot> FSEGTIOFSGLSystemRenderer::takeScreenshot() {
    return controller->takeScreenshot();
}


FSEGTIOFSGLSystemRenderer::~FSEGTIOFSGLSystemRenderer() {
}
