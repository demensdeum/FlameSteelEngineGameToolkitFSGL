#ifndef FSEGTIOFSGLSYSTEMRENDERERDELEGATE_H
#define FSEGTIOFSGLSYSTEMRENDERERDELEGATE_H


class FSEGTIOFSGLSystemRenderer;

namespace FSGL {
	class Button;
};

class FSEGTIOFSGLSystemRendererDelegate {

public:
	virtual void fsglSystemRendererDidTapButton(shared_ptr<FSEGTIOFSGLSystemRenderer> renderer, shared_ptr<Button> button) = 0;

};

#endif