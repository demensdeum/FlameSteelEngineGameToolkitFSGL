/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSEGTIOFSGLSystemRenderer.h
 * Author: demensdeum
 *
 * Created on August 5, 2017, 6:45 PM
 */

#ifndef FSEGTIOFSGLSYSTEMRENDERER_H
#define FSEGTIOFSGLSYSTEMRENDERER_H

#include <FlameSteelCore/SharedNotNullPointer.h>
#include <FSGL/Data/SystemParams.h>
#include <FlameSteelEngineGameToolkit/IO/Renderers/FSEGTRenderer.h>

#include <FSGL/Controller/FSGLController.h>
#include <FlameSteelEngineGameToolkitFSGL/Renderers/FSEGTIOFSGLSystemRendererDelegate.h>

class FSEGTIOFSGLSystem;
struct SDL_Window;

using namespace FlameSteelCore::Utils;
using namespace Shortcuts;

class FSEGTIOFSGLSystemRenderer: public FSEGTRenderer, public enable_shared_from_this<FSEGTIOFSGLSystemRenderer>, public FSGLControllerDelegate {
public:
    FSEGTIOFSGLSystemRenderer();
    virtual ~FSEGTIOFSGLSystemRenderer();

	FSEGTIOFSGLSystemRendererDelegate *delegate = nullptr;

    void preInitialize();
    void fillParams(shared_ptr<IOSystemParams> params);
    void initializeWindow(shared_ptr<IOSystemParams> params);

    virtual void setWindowTitle(string title);
    virtual void render(shared_ptr<FSEGTGameData> gameData);

    virtual void objectsContextObjectAdded(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object);
    virtual void objectsContextObjectUpdate(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object);
    virtual void objectsContextAllObjectsRemoved(shared_ptr<FSEGTObjectsContext> context);
    virtual void objectsContextObjectRemoved(shared_ptr<FSEGTObjectsContext> context, shared_ptr<Object> object);
	virtual void fsglControllerDidTapButton(shared_ptr<FSGLController> core, shared_ptr<Button> button);

    SDL_Window *window = nullptr;

    virtual shared_ptr<Screenshot> takeScreenshot();

private:
    shared_ptr<FSGLController> controller;
    NotNull<SystemParams> params;

};

#endif /* FSEGTIOFSGLSYSTEMRENDERER_H */

